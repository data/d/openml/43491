# OpenML dataset: Arcade-Game-Stats

https://www.openml.org/d/43491

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Statistics on a Blockbreaker-like Game
The author is in the process of creating a blockbreaker-like game, in which the jumping-off point is the "Block Breaker" section of the Udemy course, Complete C Unity Developer 2D: Learn to Code Making Games
After making lots of levels, the author needed to sort them by difficulty.  How does one measure the difficulty of a level?  A first-cut solution is 
to make an auto-play bot that is not perfect, and see how well the bot does on each level, using thousands of trials.
Here is a video of the game in auto-play action.
Fields

Date: date and time the game was auto-played
Level: the name of the level (the 3-digit number is an estimate of the difficulty from a previous run, no longer valid after tweaking)
NumBlocks: how many blocks have to be broken to win the level
IsWin: True if autoplay broke all the blocks, False if the ball fell past the paddle.
ElapsedTime: Seconds until either won or lost (game is played at 4x speed, so multiply by 4 to get an estimate of how long a human might play it)
Score: total score when the game was won or lost
Accuracy: the autoplay is tuned with a randomly-chosen accuracy.  Higher numbers are more likely to win.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43491) of an [OpenML dataset](https://www.openml.org/d/43491). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43491/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43491/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43491/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

